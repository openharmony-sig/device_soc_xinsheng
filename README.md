# device_soc_xinsheng

-   [简介](#简介)
-   [目录结构](#目录结构)
-   [系统要求](#系统要求)
-   [安装工具](#安装工具)
-   [编译环境HB安装](#编译环境HB安装)
-   [安装编译工具链](#安装编译工具链)
-   [源码获取](#源码获取)
-   [工程编译](#工程编译)
-   [烧录固件](#烧录固件)
-   [相关仓库](#相关仓库)

## 简介
本仓主要存放CM32M4xxR芯片系列的SDK以及Openharmony的适配代码。在Openharmony架构中，该仓主要起基础性的支撑以及适配LITEOS的作用，各厂商在使用同一系列SoC，开发不同的device或board时，可共用该仓库代码进行开发。

## 目录结构
```bash
device_soc_xinsheng
xs
    ├── BUILD.gn 										# GN构建脚本
    ├── cm32m4xxr										# CM32M4xxR芯片适配目录
    │   ├── BUILD.gn									# GN构建脚本
    │   ├── Kconfig.liteos_m.defconfig.cm32m4xxr		# CM32M4xxR Kconfig默认配置
    │   ├── Kconfig.liteos_m.defconfig.series			# series Kconfig默认配置
    │   ├── Kconfig.liteos_m.series						# series Kconfig配置项
    │   ├── Kconfig.liteos_m.soc						# 芯片 Kconfig配置项
    │   ├── adapter										# 系统相关适配
    │   ├── cm32m4xxr_sdk								# CM32M4xxR芯片相关的SDK
    │   ├── hcs											# 设备描述文件
    │   ├── hdf											# 设备框架目录
    │   └── liteos_m									# SoC 相关的系统启动文件
    ├── End User License Agreement.md					# 终端用户证书协议
    ├── Kconfig.liteos_m.defconfig						# liteos_m Kconfig默认配置
    ├── Kconfig.liteos_m.series							# liteos_m series配置项
    ├── Kconfig.liteos_m.soc							# liteos_m 芯片配置项
    ├── LICENSE											# 证书文件
    ├── OAT.xml											# 开源仓审查规则配置文件
    ├── README.md										# 中文版ReadMe
    ├── README.en.md									# 英文版ReadMe
    └── util											# 工具项目录
        └── util.gni									#工具项配置，配置一些生成固件的命令
```

## 系统要求

1. Windows 10操作系统及以上版本，用于固件烧录;
2. Ubuntu 18.04 LTS版本, 用于代码编译。

## 安装工具

1. repo工具安装： Repo是谷歌用python脚本写的调用git的一个脚本，可以实现管理多个git库。可用于打包获取openharmony的源码
   安装过程：
    在根目录下创建reop安装目录：
    mkdir ~/bin
    下载repo至安装目录：
    curl -s https://gitee.com/oschina/repo/raw/fork_flow/repo-py3  > ~/bin/repo
    修改权限：
    chmod a+x ~/bin/repo
    配置环境变量：
    sed -i '$aexport PATH=~/bin:~/.local/bin:$PATH' ~/.bashrc

2. Python3工具安装：
   在linux终端输入如下命令查看Python3版本：
   python3 --version
   根据项目要求安装版为python3.8,如所使用版本不低于3.8则不需要下载：
   
   python3.8版本下载方式：
   sudo apt-get install python3.8

   设置python和python3软链接为python3.8：
   sudo update-alternatives --install /usr/bin/python python /usr/bin/python3.8 1
   sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.8 1

   安装并升级Python包管理工具（pip3），任选如下一种方式。
   - **命令行方式：**
     sudo apt-get install python3-setuptools python3-pip -y
     sudo pip3 install --upgrade pip
   - **安装包方式：**
     curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
     python get-pip.py

3. 安装所需库和工具：
   - 使用如下apt-get命令安装编译所需的必要的库和工具：
    sudo apt-get install build-essential gcc g++ make zlib* libffi-dev e2fsprogs pkg-config flex bison perl bc openssl libssl-dev libelf-dev libc6-dev-amd64 binutils binutils-dev libdwarf-dev u-boot-tools mtd-utils gcc-arm-linux-gnueabi

## 编译环境HB安装
1. 安装hb之前请先查看python版本，确保以安装python3.8版本及以上，详情请看phthon3工具安装。
2. 执行如下命令安装hb。
   -------
   python3 -m pip install --user ohos-build
   -------
3. 配置环境变量
   -------
   vim ~/.bashrc
   -------

   将以下命令拷贝到.bashrc文件的最后一行，保存并退出。
   -------
   export PATH=~/.local/bin:$PATH
   -------

   执行如下命令更新环境变量。

   -------
   source ~/.bashrc
   -------
4. 执行"hb -h"，输出如下打印即表示安装成功：
   usage: hb [-h] [-v] {build,set,env,clean} ...

   OHOS Build System version 0.4.6

   positional arguments:
   {build,set,env,clean}
     build               Build source code
     set                 OHOS build settings
     env                 Show OHOS build env
     clean               Clean output

   optional arguments:
   -h, --help            show this help message and exit
   -v, --version         show program's version number and exit

## 安装编译工具链
1. 下载工具链: 
   用户可以[点击下载该编译链工具](下载网站: https://www.nucleisys.com/download.php),下载到用户自己工程指定的目录中,并解压。

2. 将工具链的路径加入环境变量。

## 源码获取
 - 用户可参考如下网址进行源码获取或根据下列步骤进行源码的获取。
   官方文档: OpenHarmoney文档《搭建轻量与小型系统》(https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-lite-env-setup.md)。

 - 源码下载步骤 
 1. 下载源码之前，请先创建一个目录用于存放源码(用户可以自行指定为其他目录),如下示例：
    --------
    mkdir ./openharmony
    --------
2. 源码下载
    --------
    cd openharmony
    --------
    repo init -u https://gitee.com/openharmony/manifest --no-repo-verify

3. 下载当前代码分支
   ---------
   repo sync -c

4. 下载完成后，下载大容量二进制文件。
   ---------
   repo forall -c 'git lfs pull'

## 工程编译
1. cd 进入到源码根目录，执行如下命令编译工程：
    hb set  出现如下界面
    ...
    ...
    ub
      led_demo
      task_demo
      xts_demo
    ...
    ...
    选择当前需要编译的demo
    选择完成后执行
    set build 
2. 编译成功后在out目录下有对应的demo文件出现，led_demo.bin led_demo.bin led_demo.bin。

3. 也可执行下面命令进行编译：
   ./build.sh --product-name  xxx_demo

## 固件下载

该方式适用于CM32M4xxR系列芯片型号。
工具下载方式：登入中移芯昇科技有限公司官方网站，找到工具下载，下载命名为CM32M4xxR Download Tool的工具，点击运行
操作步骤: 
1. 运行工具 连接串口，串口连接成功后，左下角会出现如下字样，串口已经打开.... 等字样。
2. 点击界面中的获取芯片信息按钮，正常情况下输出界面会有获取芯片信息执行成功字样。
3. 导入正确编译生成的bin文件，起始地址默认为 0x08000000，如有需要请自行修改。点击下载按钮，下载成功后，
   右侧 log 区显示“全部数据 crc 校验成功”。如未下载成功进度条不会显示 100%，且文本框中显示烧写失败信息。
4. 下载完成后，点击跳转按钮，芯片从用户 bin 文件起始地址开始执行，右侧文本框显示跳转成功或失败信息。

## 相关仓库

[vendor_ubtech](https://gitee.com/openharmony-sig/vendor_ubtech)

[device_board_ubtech](https://gitee.com/openharmony-sig/device_board_ubtech)
