# device_soc_xinsheng

-   [Introduction](#Introduction)
-   [Directory Structure](#Directory Structure)
-   [System Requirements](#System Requirements)
-   [installation tool](#installation tool)
-   [Compile environment HB installation](#Compile environment HB installation)
-   [Install the build toolchain](#Install the build toolchain)
-   [Source code acquisition](#Source code acquisition)
-   [project compilation](#project compilation)
-   [Firmware download](#Firmware download)
-   [Relevant warehouse](#Relevant warehouse)

## Introduction
This warehouse mainly stores the SDK of the CM32M4xxR chip series and the adaptation code of Hongmeng.In the Openharmony architecture, the warehouse mainly plays the role of basic support and adaptation to LITEOS. When manufacturers use the same series of SoCs and develop different devices or boards, they can share the warehouse code for development.

## Directory Structure
```bash
device_soc_xinsheng
xs
    ├── BUILD.gn 										# GN build script
    ├── cm32m4xxr										# CM32M4xxR Chip Adaptation Catalog
    │   ├── BUILD.gn									# GN build script
    │   ├── Kconfig.liteos_m.defconfig.cm32m4xxr		# CM32M4xxR Kconfig default allocation
    │   ├── Kconfig.liteos_m.defconfig.series			# series Kconfig default allocation
    │   ├── Kconfig.liteos_m.series						# series Kconfig configuration item
    │   ├── Kconfig.liteos_m.soc						# soc Kconfig configuration item
    │   ├── adapter										# System related adaptation
    │   ├── cm32m4xxr_sdk								# CM32M4xxR Chip related SDK
    │   ├── hcs											# device description file
    │   ├── hdf											# Device Framework Directory
    │   └── liteos_m									# SoC Related system startup files
    ├── End User License Agreement.md					# End User Certificate Protocol
    ├── Kconfig.liteos_m.defconfig						# liteos_m Kconfig default allocation
    ├── Kconfig.liteos_m.series							# liteos_m series configuration item
    ├── Kconfig.liteos_m.soc							# liteos_m SOC configuration item
    ├── LICENSE											# certificate file
    ├── OAT.xml											# Open source warehouse review rule configuration file
    ├── README.md										# Chinese version ReadMe
    ├── README_zh.md									# English Version ReadMe
    └── util											# Tool Catalog
        └── util.gni									# Tool item configuration, configure some commands to generate firmware
```

## System Requirements

1. Windows 10 operating system and above, for firmware burning;
2. Ubuntu 18.04 LTS version for code compilation.

## installation tool

1. repo tool installation: Repo is a script written by Google in python to call git, which can manage multiple git libraries.Can be used to package the source code of openharmony。
   Installation process:
    Create a reop installation directory in the root directory:
    mkdir ~/bin

    Download the repo to the installation directory:
    curl -s https://gitee.com/oschina/repo/raw/fork_flow/repo-py3  > ~/bin/repo

    edit permission:
    chmod a+x ~/bin/repo

    Configure environment variables:
    sed -i '$aexport PATH=~/bin:~/.local/bin:$PATH' ~/.bashrc

2. Python3 tool installation:
   Enter the following command in the linux terminal to check the Python3 version:
   python3 --version

   According to the project requirements, the installation version is python3.8. If the version used is not lower than 3.8, you do not need to download:
   
   How to download python3.8 version:
   sudo apt-get install python3.8

   Set python and python3 soft links to python3.8:
   sudo update-alternatives --install /usr/bin/python python /usr/bin/python3.8 1
   sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.8 1

   Install and upgrade the Python package management tool (pip3), choose one of the following methods.
   - **Command line mode:**
     sudo apt-get install python3-setuptools python3-pip -y
     sudo pip3 install --upgrade pip
   - **How to install the package:**
     curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
     python get-pip.py

3. Install the required libraries and tools:
   - Use the following apt-get command to install the necessary libraries and tools for compilation:
    sudo apt-get install build-essential gcc g++ make zlib* libffi-dev e2fsprogs pkg-config flex bison perl bc openssl libssl-dev libelf-dev libc6-dev-amd64 binutils binutils-dev libdwarf-dev u-boot-tools mtd-utils gcc-arm-linux-gnueabi

## Compile environment HB installation
1. Please check the python version before installing hb, and make sure to install python3.8 and above. For details, see phthon3 tool installation.
2. Execute the following command to install hb.
   -------
   python3 -m pip install --user ohos-build
   -------
3. Configure environment variables
   -------
   vim ~/.bashrc
   -------

   Copy the following command to the last line of the .bashrc file, save and exit.
   -------
   export PATH=~/.local/bin:$PATH
   -------

   Run the following command to update environment variables.

   -------
   source ~/.bashrc
   -------
4. Execute "hb -h" and the following output will indicate that the installation is successful:
   usage: hb [-h] [-v] {build,set,env,clean} ...

   OHOS Build System version 0.4.6

   positional arguments:
   {build,set,env,clean}
     build               Build source code
     set                 OHOS build settings
     env                 Show OHOS build env
     clean               Clean output

   optional arguments:
   -h, --help            show this help message and exit
   -v, --version         show program's version number and exit

## Install the build toolchain
1. Download the toolchain:
   Users can [click to download the compilation chain tool](download site: https://www.nucleisys.com/download.php),Download it to the directory specified by the user's own project, and decompress it.

2. Add the path to the toolchain to the environment variable.

## Source code acquisition
 - Users can refer to the following website to obtain the source code or obtain the source code according to the following steps.
   official documentation: OpenHarmoney document《Build lightweight and small systems》(https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-lite-env-setup.md)。

 - Source code download steps
 1. Before downloading the source code, please create a directory to store the source code (users can specify other directories by themselves), as shown in the following example:
    --------
    mkdir ./openharmony
    --------
2. Source code download
    --------
    cd openharmony
    --------
    repo init -u https://gitee.com/openharmony/manifest --no-repo-verify

3. Download the current code branch
   ---------
   repo sync -c

4. Once the download is complete, download the bulk binary.
   ---------
   repo forall -c 'git lfs pull'

## project compilation
1. cd Go to the source code root directory and execute the following command to compile the project:
    hb set  The following interface appears
    ...
    ...
    ub
      led_demo
      task_demo
      xts_demo
    ...
    ...
    Select the demo that needs to be compiled currently
    Execute after selection is complete
    set build 
2. After the compilation is successful, the corresponding demo file will appear in the out directory.led_demo.bin led_demo.bin led_demo.bin。

3. You can also execute the following command to compile:
   ./build.sh --product-name  xxx_demo

## Firmware download

This method is applicable to CM32M4xxR series chip models.
Tool download method：Log in to the official website of China Mobile Xinsheng Technology Co., Ltd. and find the tool to download,Download the tool named CM32M4xxR Download Tool and click Run
Steps: 
1. Run the tool to connect to the serial port. After the serial port is connected successfully, the following words will appear in the lower left corner, the serial port has been opened.... and so on.
2. Click the Get Chip Information button in the interface. Under normal circumstances, the output interface will have the words Get Chip Information Executed Successfully.
3.Import the correctly compiled bin file, the default starting address is 0x08000000, if necessary, please modify it yourself. Click the download button, after the download is successful，
  The log area on the right shows "All data crc verification is successful". If the download is not successful, the progress bar will not display 100%, and the text box will display the programming failure information.
4. After the download is complete, click the jump button, the chip starts to execute from the starting address of the user bin file, and the text box on the right displays the jump success or failure information.

## Relevant warehouse

[vendor_ubtech](https://gitee.com/openharmony-sig/vendor_ubtech)

[device_board_ubtech](https://gitee.com/openharmony-sig/device_board_ubtech)