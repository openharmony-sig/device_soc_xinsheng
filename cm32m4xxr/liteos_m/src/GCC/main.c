/*******************************************************************************
*
* COPYRIGHT(c) 2020, China Mobile IOT
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*	1. Redistributions of source code must retain the above copyright notice,
*	   this list of conditions and the following disclaimer.
*	2. Redistributions in binary form must reproduce the above copyright notice,
*	   this list of conditions and the following disclaimer in the documentation
*	   and/or other materials provided with the distribution.
*	3. Neither the name of China Mobile IOT nor the names of its contributors
*	   may be used to endorse or promote products derived from this software
*	   without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*******************************************************************************/

/**
 * @file main.c
 * @author CMIOT Firmware Team
 * @version v1.0.0
 *
 * @copyright Copyright (c) 2020, CMIOT. All rights reserved.
 */
#include <stdio.h>
#include <string.h>
#include "nuclei_sdk_hal.h"
#include "target_config.h"
#include "led.h"
#include "key.h"
#include "los_mux.h"
#include "los_task.h"

#include <stdlib.h>
#include <fcntl.h>
#include <sys/mount.h>
#include <sys/stat.h>
#include <unistd.h>

#include <hiview_log.h>
#include <hiview_output_log.h>
#include <devmgr_service_start.h>
#include "log.h"

#include "los_memory.h"
#include "los_config.h"
#define CN_MAINBOOT_TASK_STACKSIZE  1024 * 8
#define CM_MAINBOOT_TASK_PRIOR      15
#define CM_MAINBOOT_TASK_NAME      "AppDemoMain"

void OHOS_SystemInit(void);

VOID IoTWatchDogKick(VOID)
{
}

static UINT32 AppDemoMain(void)
{
    UINT32 ret;
    UINT32 taskID;
    TSK_INIT_PARAM_S Task_os = {0};

    Task_os.pfnTaskEntry = (TSK_ENTRY_FUNC)OHOS_SystemInit;
    Task_os.uwStackSize = CN_MAINBOOT_TASK_STACKSIZE;
    Task_os.pcName = CM_MAINBOOT_TASK_NAME;
    Task_os.usTaskPrio = CM_MAINBOOT_TASK_PRIOR;
    ret = LOS_TaskCreate(&taskID, &Task_os);
    if (ret != LOS_OK) {
        printf("LosAppInit failed! ERROR: 0x%x\r\n", ret);
    }
	return ret;
}

int main(void)
{    
    log_init(); 
    UINT32 ret;
    ret = LOS_KernelInit();
    if (ret != LOS_OK) {
        HILOG_ERROR(HILOG_MODULE_APP, "ret: %d", ret);
    }
	ret = AppDemoMain();
	if (ret != LOS_OK) {
        HILOG_ERROR(HILOG_MODULE_APP, "ret: %d", ret);
    }
	LOS_Start();
    return 0;  
}




 
