/*
 * Copyright (c) 2021 GOODIX.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <stdarg.h>
#include <stdio.h>
#include "los_debug.h"
#include "securec.h"
#include "log.h"
#include "los_interrupt.h"

#define BUFSIZE  256

int printf(const char *__restrict __format, ...)
{
    char buf[BUFSIZE] = { 0 };
    int len;
    va_list ap;
    va_start(ap, __format);
    len = vsnprintf_s(buf, sizeof(buf), BUFSIZE - 1, __format, ap);
    if (len < 0) {
        return len;
    }
    va_end(ap);
#if 1
    if (len > 0) {
        char const *s = buf;
        while (*s) {
            _put_char(*s++);
        }
    }
#else
    wifi_usart_put_string(buf);
#endif
    return len;
}


