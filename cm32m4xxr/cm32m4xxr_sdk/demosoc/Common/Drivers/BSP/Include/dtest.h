#ifndef __DTEST_H__
#define __DTEST_H__

#include <stdio.h>
#include "cm32m4xxr.h"
#include "cm32m4xxr_def.h"	/* Generic types definition */
#include "cm32m4xxr_gpio.h"
#include "cm32m4xxr_rcc.h"
#include "cm32m4xxr_usart.h"
#include "main.h"

void dtest_UART_CMD_init(void);
void printf_cmd (char *fmt, ...);

extern uint32_t dtest_value[256];
extern uint32_t dtest_result[256];

/* 测试日志默认使用uart5的printf打印，如果用例中有用到其它串口printf，uart5就使用自己单独定义
的printf_cmd,此时需要在main.h中定义usart_dut_printf
*/
#ifndef usart_dut_printf
#define printf_cmd printf
#endif

#define _CMD_UART5_

#ifdef _CMD_UART5_
#define UART_CMD            UART5
#define UART_CMD_GPIO       GPIOE
#define UART_CMD_CLK        RCC_APB1_PERIPH_UART5
#define UART_CMD_GPIO_CLK   RCC_APB2_PERIPH_GPIOE
#define UART_CMD_RxPin      GPIO_PIN_9
#define UART_CMD_TxPin      GPIO_PIN_8
#define UART_CMD_IRQn       UART5_IRQn
#define UART_CMD_IRQHandler UART5_IRQHandler
#endif


#endif //__DTEST_H__
