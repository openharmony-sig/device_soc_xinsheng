#include <stdio.h>
#include "dtest.h"

/*
板端重启
*/
void dtest_reset_board(uint32_t case_id)
{
	switch(case_id)
	{
		case 0:
			SysTimer_SoftwareReset();
			break;
	}
}

/*
查看当前版本号
*/
void dtest_ver(uint32_t case_id)
{
	switch(case_id)
	{
		case 0:
			printf_cmd("2021091601\n");
			break;
	}
}

/*
获取变量内容
*/
void get_dtest_value(uint32_t case_id)
{
	printf_cmd("dtest_value[%lu] = %lu\n", case_id, dtest_value[case_id]);
}

/*
获取变量内容
*/
void get_dtest_result(uint32_t case_id)
{
	printf_cmd("dtest_result[%lu] = %lu\n", case_id, dtest_result[case_id]);
}
