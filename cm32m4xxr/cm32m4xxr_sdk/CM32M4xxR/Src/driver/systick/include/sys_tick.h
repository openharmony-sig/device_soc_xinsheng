#ifndef _SYS_TICK_H
#define _SYS_TICK_H
#include "stdint.h"
//#include "core_feature_eclic.h"
//#include "demosoc.h"
#include "cm32m4xxr.h"

#define SYSTEM_CORE_CLOCK    (144000000) /* System clock freqency */
#define SysTimer_MTIMECTL_CLKSRC_Pos        2U                                          /*!< SysTick Timer MTIMECTL: CLKSRC bit Position */
//#define SysTimer_MTIMECTL_CLKSRC_Msk        (1UL << SysTimer_MTIMECTL_CLKSRC_Pos)       /*!< SysTick Timer MTIMECTL: CLKSRC Mask */


void systick_Init(void);
uint32_t getSystick(void);
void local_SysTimer_SetControlValue();
void local_SysTick_Config(uint64_t ticks);
void  local_SysTimer_SetLoadValue(uint32_t value);
void  local_SysTimer_SetCompareValue(uint32_t value);
void  local_ECLIC_SetShvIRQ(IRQn_Type IRQn, uint32_t shv);
void  local__ECLIC_SetLevelIRQ(IRQn_Type IRQn, uint8_t lvl_abs);
void  local_ECLIC_EnableIRQ(IRQn_Type IRQn);
void local_SysTimer_Start(void);


#endif
   




