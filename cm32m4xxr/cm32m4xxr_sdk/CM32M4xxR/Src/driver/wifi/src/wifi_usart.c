#include "wifi_usart.h"
#include "nuclei_sdk_soc.h"

void wifi_init(void)
{
    GPIO_InitType GPIO_InitStructure;
    USART_InitType USART_InitStructure;

    RCC_EnableAPB2PeriphClk(RCC_APB2_PERIPH_AFIO | WIFI_PERIPH_GPIO, ENABLE);
    WIFI_CLK_FUNC(WIFI_PERIPH, ENABLE);

    GPIO_ConfigPinRemap(WIFI_REMAP, ENABLE);

    GPIO_InitStructure.Pin        = WIFI_TX_PIN;
    GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(WIFI_GPIO, &GPIO_InitStructure);

    GPIO_InitStructure.Pin       = WIFI_RX_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(WIFI_GPIO, &GPIO_InitStructure);

    USART_InitStructure.BaudRate            = 115200;
    USART_InitStructure.WordLength          = USART_WL_8B;
    USART_InitStructure.StopBits            = USART_STPB_1;
    USART_InitStructure.Parity              = USART_PE_NO;
    USART_InitStructure.HardwareFlowControl = USART_HFCTRL_NONE;
    USART_InitStructure.Mode                = USART_MODE_TX | USART_MODE_RX;

    /* Init uart */
    USART_Init(WIFI_USARTx, &USART_InitStructure);

    /* Enable uart */
    USART_Enable(WIFI_USARTx, ENABLE);
}

void wifi_usart_put_char(int ch)
{
	USART_SendData(WIFI_USARTx, (uint8_t)ch);
	/* Loop until the end of transmission */
    while (USART_GetFlagStatus(WIFI_USARTx, USART_FLAG_TXDE) == RESET);
	
}


void wifi_usart_put_string(char *str)
{

    while(*str!='\0')
    {
      wifi_usart_put_char(*str);
      str ++;
    }
    
}




int wifi_usart_get_char(void)
{
	/* Loop until the end of transmission */
    while (USART_GetFlagStatus(WIFI_USARTx, USART_FLAG_RXDNE) == RESET);

    return USART_ReceiveData(WIFI_USARTx);
}






