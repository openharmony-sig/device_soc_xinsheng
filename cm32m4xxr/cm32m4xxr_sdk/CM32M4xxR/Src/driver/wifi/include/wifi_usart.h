#ifndef __WIFI_USART_H__
#define __WIFI_USART_H__

#define LOG_ENABLE	1



#include <stdio.h>
#include "main.h"


#define WIFI_USARTx			USART2
#define WIFI_CLK_FUNC       RCC_EnableAPB1PeriphClk
#define WIFI_PERIPH			RCC_APB1_PERIPH_USART2
#define WIFI_GPIO			GPIOD
#define WIFI_PERIPH_GPIO	RCC_APB2_PERIPH_GPIOD
#define WIFI_TX_PIN			GPIO_PIN_5
#define WIFI_RX_PIN			GPIO_PIN_6
#define WIFI_REMAP			GPIO_RMP1_USART2




void wifi_init(void);
void wifi_usart_put_char(int ch);
void wifi_usart_put_string(char *str);
int wifi_usart_get_char(void);



#endif /* __WIFI_USART_H__ */
